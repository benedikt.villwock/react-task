# react-task

You need to create a React App that show articles and the fitting comments like on a blog. The articles and the comments have their separate endpoint from which you get an array of objects. Please notice that the comment­ endpoint returns all possible comments. You need to match the comments with the corresponding articles.

Please notice that the comment-endpoint returns all possible comments.

You need to match the comments with the corresponding articles.

## Basic requirements:

- ~~Set up a React App~~
- Usage of Redux as a state manager
- Load the data from the API
- Displaying the loaded data
- Usage of git and github/gitlab/bitbucket

## Visual requirements:

- Keep the order of the API response
- An article contains a title and a body
- Show the relevant comments under an article

## Technical requirements:

- Usage of React 16
- Manage your states and loaded data in Redux
- Structure you folders according to redux
- 

## API endpoints:

#### Articles:

https://jsonplaceholder.typicode.com/posts/

#### Comments:

https://jsonplaceholder.typicode.com/comments

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
